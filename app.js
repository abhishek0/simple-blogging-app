const express = require('express'),
	  bodyParser = require('body-parser'),
	  path = require('path'),
	  mongoose = require('mongoose'),
	  cookieParser = require('cookie-parser'),
	  cookieSession = require('cookie-session'),
	  blogRoutes = require('./routes/blogRoutes'),
	  userRoutes = require('./routes/userRoutes');

let app = express(),
	hostname = '0.0.0.0',
	port = 3000;

// Connect to mongo db
const dbURI = 'mongodb+srv://manguuser:mangu123@nodestuff.8pg90.mongodb.net/nodeStuff?retryWrites=true&w=majority'
mongoose.connect(dbURI, {useNewUrlParser: true, useUnifiedTopology: true})
	.then((result) => {
		console.log("connected to db...");
		app.listen(port, hostname, () => {
			console.log("Starting server...");
			console.log(`Server started at http://127.0.0.1:${port}`);
		});
	})
	.catch((err) => console.log(err));


// Register view engine

app.set('view engine', 'ejs');

// Middlewares

app.use(bodyParser.urlencoded({extended: true}));

app.use(cookieParser());
app.use(cookieSession({
	secret: "secretstuffandshit" , 
	keys: ['session'],
	maxAge: 60*60*1000
}));

app.use(express.static('public'))

app.use((req, res, next) => {
	console.log('New request made:-')
	console.log('Host: ', req.hostname);
	console.log('Path: ', req.path);
	console.log('Method: ', req.method);
	next();
});


// ROUTES
app.get('/', (req, res) => {
	res.redirect('/blogs');
});

// Blog Routes
app.use('/blogs', blogRoutes);

// User Routes
app.use('/', userRoutes);

app.get('/about', (req, res) => {
	res.render('about', {
		title: 'About',
		email: req.session.email
	});
});




// 404 Route
// app.get('*', (req, res) => {
// 	res.sendFile(path.join(__dirname, 'views/404.html'))
// });
// OR
app.use((req, res) => {
	res.status(404).render('404', {
		title: "404",
		email: req.session.email
	});
});