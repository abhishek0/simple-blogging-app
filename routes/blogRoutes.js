const express = require('express'),
	  blogController = require('../controllers/blogController'),
	  ensureAuthenticated = require('../ensureAuthenticated');

const router = express.Router();

router.get('/', blogController.blogIndex);
router.get('/create', ensureAuthenticated, blogController.blogCreateGet);
router.get('/:id', blogController.blogDetails);
router.post('/', blogController.blogCreatePost);
router.delete('/:id', blogController.blogDelete);

module.exports = router;