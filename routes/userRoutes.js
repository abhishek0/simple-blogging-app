const express = require('express'),
	  userController = require('../controllers/userController');

const router = express.Router();

router.get('/login', userController.userLoginGet);
router.get('/register', userController.userRegisterGet);
router.get('/logout', userController.userLogoutGet);

router.post('/login', userController.userLoginPost);
router.post('/register', userController.userRegisterPost);

module.exports = router;