const Blog = require('../models/blog');

const blogIndex = (req, res) => {
	Blog.find().sort({createdAt: -1})
		.then((result) => {
			res.render('blogs/index', {
				title: 'All Blogs',
				blogs: result,
				email: req.session.email
			});
		})
		.catch((err) => {
			console.log(err);
		});
}

const blogDetails = (req, res) => {
	const id = req.params.id;
	let username;
	try {
		username = req.email.split('@')[0];
	} catch(err) {
		username = undefined
	}

	Blog.findById(id)
		.then((result) => {
			res.render('blogs/details', {
				blog: result,
				title: 'Blog Details',
				email: req.session.email,
				username: username
			});
		})
		.catch((err) => {
			res.status(404).render('404', {
				title: 'Blog Not Found',
				email: req.session.email,
			})
		})
};


const blogCreateGet = (req, res) => {
	res.render('blogs/create', {
		title: 'Write a blog',
		email: req.session.email
	});
};


const blogCreatePost = (req, res) => {
	// console.log(req.body);
	const blog = new Blog({
		title: req.body.title,
		snippet: req.body.snippet,
		body: req.body.body,
		author: req.session.email.split("@")[0]
	});

	blog.save()
		.then((result) => {
			res.redirect('/blogs');
		})
		.catch((err) => {
			console.log(err);
		});
};


const blogDelete = (req, res) => {
	const id = req.params.id;

	Blog.findByIdAndDelete(id)
		.then((result) => {
			res.json({redirect: '/blogs'});
		})
		.catch((err) => {
			console.log(err);
		});
};

module.exports = {
	blogIndex,
	blogDetails,
	blogCreateGet,
	blogCreatePost,
	blogDelete
}