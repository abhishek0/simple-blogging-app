const User = require('../models/user');


const userLoginGet = function(req, res) {
	res.render('users/login', {
		title: 'Login',
		message: "",
		email: req.session.email
	});
}


const userRegisterGet = function(req, res) {
	res.render('users/register', {
		title: 'Register',
		message: "",
		email: req.session.email
	});
}


const userLogoutGet = function(req, res) {
	if(req.session.email) {
		req.session.email = undefined;
	}
	res.redirect('/');
}


const userLoginPost = function(req, res) {
	User.find(req.body)
		.then((result) => {
			if(result) {
				req.session.email = req.body.email;
				res.redirect('/');
			} else {
				res.render('users/login', {
					title: 'Login',
					message: 'Invalid email or password!!',
					email: req.session.email
				});
			}
		})
		.catch(err => console.log(err));
}


const userRegisterPost = function(req, res) {
	User.findOne({email: req.body.email})
		.then((result) => {
			if(result) {
				res.render('users/register', {
					title: 'Register',
					message: 'Entered email already exists!!',
					email: req.session.email
				})
			} else {
				let newUser = new User(req.body);
				newUser.save()
					.then((result) => {
						res.redirect('/login');
					})
					.catch(err => console.log(err));
			}
		})
		.catch(err => console.log(err));
}

module.exports = {
	userLoginGet,
	userRegisterGet,
	userLoginPost,
	userRegisterPost,
	userLogoutGet
}